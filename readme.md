# Coffee-Server - A Raspberry Pi powered coffee maker

This is the repository of my hobby project, where I connected my coffee maker to my Raspberry Pi. This way I can control the server's HTTP API to control my coffee maker remotely with an phone app.

This is project is a remake of my older project with same functionality.


## Setup and Configuration

### Libraries

The application uses the following libraries:

* [RPi.GPIO](https://pypi.org/project/RPi.GPIO/) for controlling GPIO pins
* [Bottle](https://bottlepy.org/docs/dev/) for web server
* [bcrypt](https://pypi.org/project/bcrypt/) for checking checking credentials.

### Wiring

In my setup, the system requires two GPIO pins: one pin for the led light and second one for the relay board. These GPIO pins are defined by the variables ``LED_OUTPUT`` and ``RELAY_OUTPUT``in the *server.py*. These pins are configured to as output pins and they are set to high, when the led or relay is swtiched on.

### Database

The server uses a handcrafted sqlite database to store the user credentials. The server script does not create the necessary database and application does not have method to add new users. The database and the users must be created independently.


## API

The Coffee maker has three HTTP methods, which all are only available for authenticated users.

### GET /status

The status method returns the current status of the coffee maker. The returned JSON-object has a following format:

    {
      "power": [true|false],
      "timer": "%Y-%m-%d %H-%M",
      "timeout": "%Y-%m-%d %H-%M",
    }

* "Power": tells if the coffee maker is currently on or off.
* "Timer": If the coffee maker is set on timer, then this field will have the timestamp when the coffee maker will be turned on. If no timer is set, this field will have an empty string.
* "timeout": If the coffee maker is on, this field will have the time when the coffee maker will switch off. If the coffee maker is off, this field will have an empty string.


### PUT /power

This method can be used to explicitly switch coffee maker on or off. The request must have the *Content-type* header set to *application/json* and it must have the following json-body:

    { 
        "power": [true|false|on|off] 
    }

If the power has value true or on, then the relay and the led will switched on and the timeout timer will refreshed to its default value defined by ``CoffeeControl.DEFAULT_TIMEOUT`` assignment in line 22 in *server.py*.

If the value is false or off, then the relay and the led will switched off and all the timers will be canceled.

Any other value or missing the power field completely will return an error code 400.


### PUT /timer

This method will start the timer, which will switch the coffee maker on. The request must have the *Content-type* header set to *application/json* and it takes a following json object as a parameter.

    {
      "datetime": "%Y-%m-%d %H-%M"
    }

The datetime is the point in time when the coffee maker should turn on. The input string follows the python string date format, e.g. the string *"1970-01-02 13:30"* would switch the coffee maker on second of january in 1970 at 13:30.

If the datetime is valid and is in the future (not in the past), the server will start a timer which will switch the coffee maker on at the given time and will start blinking the led light. If the timer is started succesfully, the server will respond with status code 202.

If the datetime is in the past or is missing, then the server will return status code 400.


### PUT /timeout

This method can be used to update the coffee maker's timeout timer to power off the machine in different point of time. The method is only available if the coffee maker is already switched on and will return the status code 400 if the coffee maker is not on.

The request has the same body and headers as the ``PUT /timer``.

    {
      "datetime": "%Y-%m-%d %H-%M"
    }

The method's error handler and parser behaves very similiarly to the ``PUT /timer``. If the request is missing the headers, JSON is not valid or does not have valid datetimefield, then the method will respond the same way as the timer request would.
 