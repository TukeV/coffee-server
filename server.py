# -*- coding: utf-8 -*-
"""Startup script for the RPi-Coffee server

This script first configures the necessary parameters for the RPi-coffee server and then implements the HTTP-methods to
interact with the CoffeeControl script. All methods require basic authentication and the requests must have the
Content-type header set to application/json.
"""

from bottle import Bottle, run, auth_basic, request, response
from coffee_control import CoffeeControl
import sqlite3
import bcrypt
import RPi.GPIO as GPIO
import datetime
from json import JSONDecodeError as JSONDecodeError


# Path to the database file
database = "kahvi.db"

# GPIO pin which controls the relay
RELAY_OUTPUT = 16

# GPIO pin which powers up the led
LED_OUTPUT = 15

# Default time after relay will be switched off
CoffeeControl.DEFAULT_TIMEOUT = 1800

# Construct the controller for the devices.
coffee_control = CoffeeControl(RELAY_OUTPUT, LED_OUTPUT)

app = Bottle()


def authenticate(username, password):
    """
    Function to check if username matches the hashed value.
    NOTE: The application does not have way to create the database or add new users.
    They have to be added independently.
    """
    conn = sqlite3.connect(database)
    users = conn.execute("SELECT * FROM users WHERE user = ?",  [username])
    for user_entry in users:
        if username == user_entry[0]:
            conn.close()
            if bcrypt.checkpw(password.encode('utf-8'), user_entry[1].encode('utf-8')):
                return True
            else:
                return False
    conn.close()
    return False


@app.route('/status', method='GET')
@auth_basic(authenticate)
def get_status():
    """Returns the current status of the CoffeeControl."""
    return coffee_control.get_status()


@app.route('/timer', method='PUT')
@auth_basic(authenticate)
def start_timer():
    """
    Starts the timer, which will switch the coffee maker on at the given time. Valid request uses the following format:
    {
        "datetime": "%Y-%m-%d %H:%M" // Year-month-day hours:minutes
    }
    A successful request will return the status code 202.
    If the request...
    - Does not have the Content-type application/json -header,
    - Does not have a json object with datetime field,
    - Does now follow the above datetime format or
    - The given datetime is in the past
    then the method will return error code 400 and an explanation.
    """
    try:
        timestamp = request.json.get("datetime", None)
        timer = datetime.datetime.strptime(timestamp, "%Y-%m-%d %H:%M")
        if (timer - datetime.datetime.now()).total_seconds() > 0:
            coffee_control.start_power_timer(timer)
            response.status = 202
            response_body = coffee_control.get_status()
        else:
            response.status = 400
            response_body = {"error": "Timestamp was in the past"}
    except JSONDecodeError:
        response.status = 400
        response_body = {"error": "Invalid JSON"}
    except TypeError:
        response.status = 400
        response_body = {"error": "Request must have datetime attribute"}
    except ValueError as e:
        response.status = 400
        response_body = {"error": str(e)}
    except AttributeError:
        response.status = 400
        response_body = {"error": "No JSON found. Do you have the correct Content-type header?"}
    return response_body


@app.route('/timeout', method='PUT')
@auth_basic(authenticate)
def set_new_timeout():
    """
    Updates the timeout timer. Works only if coffee maker is on. It follows same format as the PUT /timer method.
    {
        "datetime": "%Y-%m-%d %H:%M" // Year-month-day hours:minutes
    }
    A successful request will return the status code 202.
    If the request...
    - Does not have the Content-type application/json -header,
    - Does not have a json object with datetime field,
    - Does now follow the above datetime format
    - The given datetime is in the past or
    - Is sent when coffee maker is not on
    then the method will return error code 400 and an explanation.
    """
    if not coffee_control.power:
        response.status = 400
        response_body = {"error": "The coffee maker must be on before updating the timeout timer."}
    else:
        try:
            timestamp = request.json.get("datetime", None)
            timer = datetime.datetime.strptime(timestamp, "%Y-%m-%d %H:%M")
            if (timer - datetime.datetime.now()).total_seconds() > 0:
                coffee_control.start_timeout_timer(timer)
                response.status = 202
                response_body = coffee_control.get_status()
            else:
                response.status = 400
                response_body = {"error": "Timestamp was in the past"}
        except JSONDecodeError:
            response.status = 400
            response_body = {"error": "Invalid JSON"}
        except TypeError:
            response.status = 400
            response_body = {"error": "Request must have datetime attribute"}
        except ValueError as e:
            response.status = 400
            response_body = {"error": str(e)}
        except AttributeError:
            response.status = 400
            response_body = {"error": "No JSON found. Do you have the correct Content-type header?"}
    return response_body


@app.route('/power', method='PUT')
@auth_basic(authenticate)
def set_power():
    """
    Explicitly switches the coffee maker on or off. The method takes a following JSON object as an input:
    {
        "power": ["true" | "false" | "on" | "off"]
    }
    If the power is either "on" or "true", then the coffee maker will be switched on and
    if the power is "off" or "false", then the coffee maker is powered off and all the current timers are canceled.
    If the request does not have the necessary header or does not have the correct "power" parameter, then the
    method will return with the status code 400.
    A successful request will return with status 200.
    """
    try:
        power = request.json["power"]
        power = str(power).lower()
        if power == 'on' or power == 'true':
            coffee_control.set_power(True)
            response.status = 200
            response_body = coffee_control.get_status()
        elif power == 'off' or power == 'false':
            coffee_control.set_power(False)
            response.status = 200
            response_body = coffee_control.get_status()
        else:
            response.status = 400
            response_body = {"error": "Power must be either true, false, on or off"}
    except KeyError:
        response.status = 400
        response_body = {"error": "Request must have the power attribute."}
    except JSONDecodeError:
        response.status = 400
        response_body = {"error": "Invalid JSON"}
    except ValueError as e:
        response.status = 400
        response_body = {"error": str(e)}
    except AttributeError:
        response.status = 400
        response_body = {"error": "No JSON found. Do you have the correct Content-type header?"}
    return response_body


try:
    run(app, host='0.0.0.0', port=8888)
except (RuntimeError, KeyboardInterrupt) as e:
    print(e)
finally:
    print("Cleaning up the GPIO...", end='')
    GPIO.cleanup([LED_OUTPUT, RELAY_OUTPUT])
    print('Done.')
print("End of Script")
