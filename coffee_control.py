import datetime
import RPi.GPIO as GPIO
import time
from threading import Thread, Event, Timer


class CoffeeControl:
    """
    CoffeeControl is a facade object, which encapsulates the coffee maker's behaviours. It can be used to turn
    coffee maker on and off and setup timers.

    The behaviour of this class can be easily understood as a state machine which has 3 states: ON, OFF and TIMED.
    * In OFF state, both relay and LED are switched off.
    * In ON state, the LED and relay are on and there is a timeout timer which will eventually switch
    both led and relay off.
    * In TIMED state, the led is blinking until the power-timer will switch led and the relay on.
    """

    # Default time for timeout
    DEFAULT_TIMEOUT = 10

    def __init__(self, relay_output, led_output):
        """
        Constructor for the coffee control object. The relay_output and led_output are the pins used to control
        the led and the relay.
        :param relay_output: GPIO pin for the relay
        :param led_output: GPIO pin for the led
        """
        self.relay = RelayControl(relay_output)
        self.led = LedControl(led_output)
        self.power_timer = Timer(1, lambda: None)
        self.timeout_timer = Timer(1, lambda: None)
        self.power_timestamp = None
        self.timeout_timestamp = None
        self.power = False

    @property
    def power(self):
        return self.relay.power

    @power.setter
    def power(self, power):
        self.set_power(power)

    def set_power(self, power):
        """
        Stops all the timers and either turns the led and relay on or off. If the devices are turned on, then
        this will also start a timeout timer which will eventually power off the device.
        :param power: Boolean to determine if the device should on or off. True will start a new timeout timer.
        """
        self.stop_timers()
        self.led.power(power)
        self.relay.power = power
        if power:
            self.power_timestamp = datetime.datetime.now()
            self.start_timeout_timer()
        print("{:%Y-%m-%d %H:%M} - Coffee maker switched {}.".format(datetime.datetime.now(), "on" if power else "off"))

    def start_power_timer(self, power_timestamp):
        """
        This function will start a timer, which will switch the devices on at the given datetime.
        :param power_timestamp: Datetime when the timer should trigger. If the datetime is in the past, function will
        raise a ValueError.
        """
        time2wait = (power_timestamp - datetime.datetime.now()).total_seconds()
        if time2wait > 0:
            self.power = False
            self.power_timestamp = power_timestamp
            self.power_timer = Timer(time2wait, self.set_power, [True])
            self.power_timer.start()
            self.led.start_blinking()
            print("{:%Y-%m-%d %H:%M} - Coffee maker will be switched on at  {:%Y-%m-%d %H:%M}"
                  .format(datetime.datetime.now(), self.power_timestamp))
        else:
            raise ValueError("Timer must be set into the future.")

    def start_timeout_timer(self, timeout_timestamp=None):
        """
        Starts the timeout timer, which will power off the led and the relay. The time before the timer triggers is
        defined by the DEFAULT_TIMEOUT member variable (in seconds).
        :return:
        """
        if timeout_timestamp is None:
            timeout = self.DEFAULT_TIMEOUT
            timeout_timestamp = datetime.datetime.now() + datetime.timedelta(seconds=timeout)
        else:
            timeout = (timeout_timestamp - datetime.datetime.now()).total_seconds()
            if timeout < 0:
                raise ValueError("Timeout must be set into the future")
        self.timeout_timer.cancel()
        self.timeout_timer = Timer(timeout, self.set_power, [False])
        self.timeout_timer.start()
        self.timeout_timestamp = timeout_timestamp
        print("{:%Y-%m-%d %H:%M} - Coffee maker will switch off at {:%Y-%m-%d %H:%M}"
              .format(datetime.datetime.now(), self.timeout_timestamp))

    def stop_timers(self):
        """This function cancels both timers."""
        self.power_timer.cancel()
        self.timeout_timer.cancel()
        self.power_timestamp = None
        self.timeout_timestamp = None

    def get_status(self):
        """
        Returns the status as a dictionary. The dictionary will have following format:
        - "power": [True|False], Is the device now on or off
        - "timer": ["" | "%Y-%m-%d %H:%M"], Date and time, when the power on timer will trigger.
        "" means no timer has been set.
        - "timeout: ["" | "%Y-%m-%d %H:%M"], Date and time, when the timeout on timer will trigger.
        "" means no timer has been set.
        """
        timer = self.power_timestamp
        timeout = self.timeout_timestamp
        return {
            "power": self.power,
            "timer": timer.strftime("%Y-%m-%d %H:%M") if timer is not None else "",
            "timeout": timeout.strftime("%Y-%m-%d %H:%M") if timeout is not None else ""
        }


class LedControl:
    """Class which controls the led light. Class can be used either switch the led on or off or set to blinking."""

    def __init__(self, output_pin):
        """
        Constructor of the class. This will configure the given GPIO pin to be an output pin and set it to False.
        :param output_pin: GPIO pin used to power up the Led.
        """
        self.output_pin = output_pin
        self.event = Event()
        self.blink_thread = Thread(target=self._blink_led, daemon=True)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.output_pin, GPIO.OUT)
        GPIO.output(self.output_pin, False)

    def start_blinking(self):
        """
        Start's a thread, which will blink the led. If the led is already blinking, it does nothing.
        """
        if not self.blink_thread.is_alive():
            self.blink_thread = Thread(target=self._blink_led, daemon=True)
            self.blink_thread.start()

    def stop_blinking(self):
        """
        Stops the led by setting giving the stop signal to the blink-thread and joining it.
        Does nothing if the thread is not running.
        """
        if self.blink_thread.is_alive():
            self.event.set()
            self.blink_thread.join()
        self.event.clear()

    def power(self, power):
        """
        Explicitly either switches the led on or off. If the led is blinking, then the function will first call the
        stop_blinking to stop the blinking.
        :param power: Determines if led should be switched on or off.
        """
        self.stop_blinking()
        GPIO.output(self.output_pin, power)
        print("{:%Y-%m-%d %H:%M} - Led is now {}".format(datetime.datetime.now(), "on" if power else "off"))

    def _blink_led(self):
        """
        The function, which will be ran in the blink thread. This will switch the led on and off in one
        second intervals until the event is set. When the event is set, the led is then turned off before the function
        exits.
        """
        self.event.clear()
        blinker = True
        print("{:%Y-%m-%d %H:%M} - Led is now blinking".format(datetime.datetime.now()))
        while not self.event.is_set():
            blinker = not blinker
            GPIO.output(self.output_pin, blinker)
            time.sleep(1)
        print("{:%Y-%m-%d %H:%M} - Led is no longer blinking".format(datetime.datetime.now()))
        GPIO.output(self.output_pin, False)


class RelayControl:
    """Class to control the relay."""
    def __init__(self, output_pin):
        """
        Constructor will setup the output_pin to be the pin which will control the relay.
        :param output_pin: GPIO pin used to control the relay.
        """
        self.output_pin = output_pin
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.output_pin, GPIO.OUT)
        GPIO.output(self.output_pin, False)

    @property
    def power(self):
        """Returns the current state of the relay"""
        return bool(GPIO.input(self.output_pin))

    @power.setter
    def power(self, power):
        """Function to switch the relay on or off."""
        print("{:%Y-%m-%d %H:%M} - Relay is now {}".format(datetime.datetime.now(), "on" if power else "off"))
        GPIO.output(self.output_pin, power)

